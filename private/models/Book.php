<?php

class Book extends Util{

    public $id;
    public $name;
    public $author_name;
    public $book_url;
    public $genres;
    public $image_name;
    public $resolution;
    public $description;
    public $created;
    public $admin_id;
    public $status;
    public $listening_count;

}
