<?php

$current = basename($_SERVER["SCRIPT_FILENAME"]);
$b_index = $b_users = $b_admob = $b_site_config = "";
$b_property = $b_artist = $b_book = $b_genre = $b_album = $b_track = $b_playlist = $b_push_notification = $b_add_feedback = "";

if($current == "index.php") $b_index = "active";
else if(($current == "artists.php") ||($current == "artist-form.php") || ($current == "artist-tracks.php")) $b_artist = "active";
else if(($current == "book.php") ||($current == "book-form.php") ) $b_book = "active";
else if(($current == "genres.php") ||($current == "genre-form.php") || ($current == "genre-tracks.php")) $b_genre = "active";
else if(($current == "albums.php") || ($current == "album-form.php") || ($current == "album-tracks.php")) $b_album = "active";
else if(($current == "push-messages.php") || ($current == "notification-form.php")) $b_push_notification = "active";
else if(($current == "tracks.php") ||($current == "track-form.php")) $b_track = "active";
else if($current == "users.php") $b_users = "active";
else if($current == "app-feedback.php") $b_add_feedback = "active";
else if($current == "playlists.php" || $current == "playlist-tracks.php" || $current == "playlist-form.php") $b_playlist = "active";
else if($current == "admob.php") $b_admob = "active";
else if($current == "site-config.php") $b_site_config = "active";

?>

<div class="sidebar" id="sidebar">
    <ul class="sidebar-list">
        <li class="<?php echo $b_index; ?>"><a href="index.php"><i class="ion-ios-pie"></i><span>Dashboard</span></a></li>
        <li class="<?php echo $b_book; ?>"><a href="book.php"><i class="ion-ios-book"></i><span>Book</span></a></li>
        <li class="<?php echo $b_artist; ?>"><a href="artists.php"><i class="ion-android-person"></i><span>Artists</span></a></li>
        <li class="<?php echo $b_genre; ?>"><a href="genres.php"><i class="ion-android-apps"></i><span>Genres</span></a></li>
        <li class="<?php echo $b_album; ?>"><a href="albums.php"><i class="ion-ios-albums"></i><span>Albums</span></a></li>
        <li class="<?php echo $b_track; ?>"><a href="tracks.php"><i class="ion-ios-musical-notes"></i><span>Tracks</span></a></li>
        <li class="<?php echo $b_playlist; ?>"><a href="playlists.php"><i class="ion-ios-musical-note"></i><span>Playlists</span></a></li>
        <li class="<?php echo $b_users; ?>"><a href="users.php"><i class="ion-person"></i><span>Users</span></a></li>
        <li class="<?php echo $b_admob; ?>"><a href="admob.php"><i class="ion-closed-captioning"></i><span>Admob</span></a></li>
        <li class="<?php echo $b_push_notification; ?>"><a href="push-messages.php"><i class="ion-ios-bell"></i><span>Push Notification</span></a></li>
        <li class="<?php echo $b_add_feedback; ?>"><a href="app-feedback.php"><i class="ion-chatbubbles"></i><span>App Feedback</span></a></li>
        <li class="<?php echo $b_site_config; ?>"><a href="site-config.php"><i class="ion-settings"></i><span>Configuration</span></a></li>
    </ul>
</div><!--sidebar-->
