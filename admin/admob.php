<?php require_once('../private/init.php'); ?>

<?php

$admin = Session::get_session(new Admin());

if(empty($admin)) Helper::redirect_to("login.php");

?>

<?php require("common/php/php-head.php"); ?>

<body>

<?php require("common/php/header.php"); ?>

<div class="main-container">

    <?php require("common/php/sidebar.php"); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <ul class="ajax-sidebar">
                <li class="head">Admob</li>
                <li><a href="#<?php echo ADMOB_BANNER; ?>">Banner</a></li>
                <li><a href="#<?php echo ADMOB_INTERSTITIAL; ?>">Interstitial</a></li>
            </ul>

            <div class="ajax-form-wrapper loader-wrapper">
                <div class="btn-loader loader-big"><span class="active ajax-loader"><span></span></span></div>

                <form class="ajax-form tab-form banner" id="<?php echo ADMOB_BANNER; ?>"
                      method="post" data-url="<?php echo ADMOB_BANNER_API; ?>">

                    <input type="hidden" name="id" value="<?php echo Helper::get_val("id"); ?>"/>
                    <a href="#" class="head ajax-sidebar-dropdown">
                        <h5 class="title">Banner Admob</h5>
                        <span class="dropdown-icon"><i class="ion-android-arrow-dropdown"></i></span>
                    </a>

                    <div class="item-content">
                        <div class="ajax-bar"></div>

                        <h5 class="mt-10 mb-30 ajax-message"></h5>

                        <input type="hidden" name="id" value="<?php echo Helper::get_val("id"); ?>"/>
                        
                        <label>Banner ID</label>
                        <input type="text" data-ajax-field="true" placeholder="eg. ca-app-pub-3940256099942544~334751171" name="banner_id" />
                        
                        <label>Banner Unit ID</label>
                        <input type="text" data-ajax-field="true" placeholder="eg. ca-app-pub-3940256099942544/6300978111" name="banner_unit_id" />
                        
                        <div class="btn-wrapper">
                            <a href="#" class="float-l oflow-hidden mt-5">
                                <label class="status switch">
                                    <input type="checkbox" name="banner_status" />
                                     <span class="slider round">
                                        <b class="active">Active</b>
                                        <b class="inactive">Inactive</b>
                                    </span>
                                </label>
                                <span class="toggle-title"></span>
                            </a>

                            <button type="submit" class="c-btn mb-10"><b>Save</b></button>
                        </div>
                    </div><!--item-content-->
                </form>


                <form class="ajax-form tab-form interstitial" id="<?php echo ADMOB_INTERSTITIAL; ?>"
                      method="post" data-url="<?php echo ADMOB_INTERSTITIAL_API; ?>">

                    <input type="hidden" name="id" value="<?php echo Helper::get_val("id"); ?>"/>
                    <a href="#" class="head ajax-sidebar-dropdown">
                        <h5 class="title">Interstitial Admob</h5>
                        <span class="dropdown-icon"><i class="ion-android-arrow-dropdown"></i></span>
                    </a>

                    <div class="item-content">
                        <div class="ajax-bar"></div>
                        <h5 class="mt-10 mb-30 ajax-message"></h5>

                        <input type="hidden" name="id" value="<?php echo Helper::get_val("id"); ?>"/>
                        
                        <label>Interstitial ID</label>
                        <input type="text" data-ajax-field="true" placeholder="eg. ca-app-pub-3940256099942544~334751171" name="interstitial_id" />

                        <label>Interstitial Unit ID</label>
                        <input type="text" data-ajax-field="true" placeholder="eg. ca-app-pub-3940256099942544/6300978111" name="interstitial_unit_id" />
                        
                        <div class="btn-wrapper">
                            <a href="#" class="float-l oflow-hidden mt-5">
                                <label class="status switch">
                                    <input type="checkbox" name="interstitial_status" />
                                     <span class="slider round">
                                        <b class="active">Active</b>
                                        <b class="inactive">Inactive</b>
                                    </span>
                                </label>
                                <span class="toggle-title"></span>
                            </a>

                            <button type="submit" class="c-btn mb-10"><b>Save</b></button>
                        </div>
                    </div><!--item-content-->
                </form>
                
            </div><!--ajax-form-wrapper loader-wrapper-->
            
        </div><!--main-content-inner-->
    </div><!--main-content-->
</div><!--main-container-->


<?php require("common/php/php-footer.php"); ?>

<script>

    /*MAIN SCRIPTS*/
    (function ($) {
        "use strict";

        ajaxSidebarInit(window.location.hash, '<?php echo ADMOB_BANNER; ?>');

    })(jQuery);

</script>

