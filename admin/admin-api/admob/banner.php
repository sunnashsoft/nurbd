<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $admob = new Admob();
        $admob = $admob->where(["admin_id" => $admin->id])->one();

        if(!empty($admob)){

            $text_field["id"] = $admob->id;
            $text_field["banner_id"] = $admob->banner_id;
            $text_field["banner_unit_id"] = $admob->banner_unit_id;

            $response_obj["banner"]["text"] = $text_field;
            $response_obj["banner"]["switch"]["banner_status"] = $admob->banner_status;

            $response->create(200, "Success", $response_obj);

        }else $response->create(201, "Invalid", null);

    }else if(Helper::is_post()){
        $admob = new Admob();
        $admob->id = Helper::post_val("id");
        $admob->banner_id = Helper::post_val("banner_id");
        $admob->banner_unit_id = Helper::post_val("banner_unit_id");

        $admob->banner_status = (isset($_POST['banner_status'])) ? 1 : 2;

        $admob->admin_id = $admin->id;
        $admob->validate_with(["banner_id", "banner_unit_id", "banner_status"]);
        $errors = $admob->get_errors();
        $success = false;

        if($errors->is_empty()){
            if($admob->id){
                if($admob->where(["id" => $admob->id])->update()) $success = true;
            }else{
                $admob->id = $admob->save();
                if($admob->id > 0) {
                    $response_obj["redirect"] = "id=" . $admob->id;
                    $success = true;
                }
            }

            if($success){

                $response_obj["banner"]["switch"]["banner_status"] = $admob->banner_status;
                $response_obj["banner"]["text"]["banner_id"] = $admob->banner_id;
                $response_obj["banner"]["text"]["banner_unit_id"] = $admob->banner_unit_id;

                $response->create(200, "Success", $response_obj);

            }else $response->create(201, "Something Went Wrong", null);
        }else $response->create(201, "Required Field is missing", null);

    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>
