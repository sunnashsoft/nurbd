<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $page = Helper::get_val("page");
        if(!$page) $page = 1;

        $start = ($page - 1) * BACKEND_PAGINATION;

        $search = Helper::get_val("search");
        $sort = Helper::get_val("sort");
        $sort_type = Helper::get_val("sort_type");

        if(!$sort_type) $sort_type = "DESC";

        $total_notification = new Push_Notification();

        $has_search = ($search && ($search != "") && ($search != null));
        $has_sort = ($sort && ($sort != "") && ($sort != null));


        if($has_search){
            $total_notification = $total_notification->where(['admin_id' => $admin->id])->like(["title" => $search])->search()->count();
        } else {
            $total_notification = $total_notification->where(['admin_id' => $admin->id])->count();
        }
        
        

        $response_values = [];

        $response_obj["total"] = $total_notification;
        $response_obj["page"] = $page;
        $response_obj["page_item"] = BACKEND_PAGINATION;

        $notifications = new Push_Notification();

        if($has_search && $has_sort){
            $notifications = $notifications->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->like(["title" => $search])->search()->orderBy($sort)->orderType($sort_type)->all();

        }else if($has_sort){
            $notifications = $notifications->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->orderBy($sort)->orderType($sort_type)->all();

        }else if($has_search){
            $notifications = $notifications->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->like(["title" => $search])->search()
                ->orderBy("created")->orderType("DESC")
                ->all();

        } else {
            $notifications = $notifications->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->orderBy("created")->orderType("DESC")
                ->all();
        }

        $response_obj["current_item_count"] = count($notifications);


        foreach ($notifications as $item){

            $track_count = new Track();
            $track_count = $track_count->where(['admin_id' => $admin->id])->count();

            $current_message["delete"] = $item->id;
            $current_message["edit"] = $item->id;
            $current_message["notify"] = $item->id;

            $current_values["title"]["text"] = $item->title;
            $current_values["message"]["text"] = $item->message;
            $current_values["created"]["text"] = Helper::days_ago($item->created);

            $current_message["values"] = $current_values;

            array_push($response_values, $current_message);
        }

        $response_obj["head"] = ["Title" => [ "title", "DESC" ],
            "Message" => null, "Created" => [ "created", "ASC" ], "" => null ];

        $response_obj["body"] = $response_values;

        $response->create(200, "Success", $response_obj);

    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>