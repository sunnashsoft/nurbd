<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $id = Helper::get_val("id");
        $response_obj["push_notification"]["text"] = [];

        if($id){
            $push_notification = new Push_Notification();
            $push_notification = $push_notification->where(["id" => $id])->one("id, title, message");

            $text_field["title"] = $push_notification->title;
            $text_field["message"] = $push_notification->message;

            $response_obj["push_notification"]["text"] = $text_field;
        }

        $response->create(200, "Success", $response_obj);

    }else if(Helper::is_post()){
        $push_notification = new Push_Notification();
        $push_notification->id = Helper::post_val("id");

        $push_notification->title = Helper::post_val("title");
        $push_notification->message = Helper::post_val("message");

        $push_notification->admin_id = $admin->id;

        $push_notification->validate_with(["title", "message", "admin_id"]);
        $errors = $push_notification->get_errors();
        $success = false;

        if($errors->is_empty()){
            if($push_notification->id){
                if($push_notification->where(["id" => $push_notification->id])->update()) $success = true;
            }else{
                $push_notification->created = date(DATE_FORMAT);
                $push_notification->id = $push_notification->save();
                if($push_notification->id > 0) {
                    $response_obj["redirect"] = "id=" . $push_notification->id;
                    $success = true;
                }
            }

            if($success){
                $push_notification->admin_id = null;

                $response_obj["push_notification"]["text"] = $push_notification->to_valid_array();
                $response->create(200, "Success", $response_obj);

            }else $response->create(201, "Something Went Wrong", null);
        }else $response->create(201, "Required Field is missing", null);
    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>