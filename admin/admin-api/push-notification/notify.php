<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());


if(!empty($admin)) {
    if(Helper::is_post()){

        $message_id = Helper::post_val("id");

        if($message_id){

            $pus_notification = new Push_Notification();
            $pus_notification = $pus_notification->where(["id" => $message_id])->one("title, message");

            $site_config = new Site_Config();
            $site_config = $site_config->where(["admin_id" => $admin->id])->one("firebase_auth");


            if(!empty($site_config) && !empty($pus_notification)){
                $url = 'https://fcm.googleapis.com/fcm/send';

                $mes['title'] = $pus_notification->title;
                $mes['desc'] = $pus_notification->message;

                $fields = array(
                    'to' => '/topics/message',
                    'data' => array(
                        "message" => $mes,
                    )
                );

                $fields = json_encode($fields);

                $headers = array(
                    'Authorization:key =' . $site_config->firebase_auth,
                    'Content-Type: application/json',
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

                $result = curl_exec($ch);

                curl_close($ch);
                $res_data = json_decode($result);

                if(!empty($res_data)) $response->create(200, "Notified Successfully.", null);
                else $response->create(201, "Something Went Wrong.", null);

            }else $response->create(201, "Invalid Notification/Firebase Auth", null);
        }else $response->create(201, "Invalid Parameter", null);
    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();



?>