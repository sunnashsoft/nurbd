<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $id = Helper::get_val("id");

        if($id){
            $push_notification = new Push_Notification();
            $push_notification = $push_notification->where(["id" => $id])->one("admin_id");
            
            if(!empty($push_notification)){
                if($push_notification->admin_id == $admin->id){

                    $delete_notification = new Push_Notification();
                    if($delete_notification->where(["id" => $id])->delete()){

                        $response->create(200, "Success", $delete_notification);

                    }else $response->create(201, "Something went wrong. Please try again.", null);
                }else $response->create(201, "You are unable to delete this notification", null);
            }else $response->create(201, "Invalid Notification", null);
        }else $response->create(201, "Invalid Notification", null);
    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>