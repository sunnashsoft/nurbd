<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());
$upload_folder = "../../" . UPLOADED_DIR . "/" . AUDIO_DIR . "/";

if(!empty($admin)) {
    if(Helper::is_get()){

        $id = Helper::get_val("id");

        if($id){
            $artist = new Book();
            $artist = $artist->where(["id" => $id])->one("image_name, admin_id");

            if(!empty($artist)){
                if($artist->admin_id == $admin->id){

                    $delete_artist = new Book();
                    if($delete_artist->where(["id" => $id])->delete()){

                        Upload::delete($upload_folder, $artist->image_name);
                        $response->create(200, "Success", $artist);

                    }else $response->create(201, "Something went wrong. Please try again.", null);
                }else $response->create(201, "You are unable to delete this artist", null);
            }else $response->create(201, "Invalid Book", null);
        }else $response->create(201, "Invalid Book", null);
    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>
