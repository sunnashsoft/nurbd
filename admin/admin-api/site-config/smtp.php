<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $smtp_config = new Smtp_Config();
        $smtp_config = $smtp_config->where(["admin_id" => $admin->id])->one();

        $response_obj["smtp_config"]["text"] = $smtp_config->to_valid_array();

        $response->create(200, "Success", $response_obj);

    } else if(Helper::is_post()){

        $smtp_config = new Smtp_Config();
        $id = Helper::post_val("id");

        $smtp_config->host = Helper::post_val("host");
        $smtp_config->sender_email = Helper::post_val("sender_email");
        $smtp_config->username = Helper::post_val("username");
        $smtp_config->smtp_password = Helper::post_val("smtp_password");
        $smtp_config->port = Helper::post_val("port");
        $smtp_config->encryption = Helper::post_val("encryption");

        if($smtp_config->where(["id" => $id])->update()){

            $response_obj["smtp_config"]["text"] = $smtp_config->to_valid_array();
            $response->create(200, "Success", $response_obj);


        }else $response->create(201, "Something went wrong.", null);
    } else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();
?>