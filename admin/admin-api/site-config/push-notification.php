<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $site_config = new Site_Config();
        $site_config = $site_config->where(["admin_id" => $admin->id])->one("id, firebase_auth");
        $site_config->resolution = null;

        $response_obj["firebase_push"]["text"] = $site_config->to_valid_array();
        $response->create(200, "Success", $response_obj);

    } else if(Helper::is_post()){

        $site_config = new Site_Config();
        $id = Helper::post_val("id");
        $site_config->firebase_auth = Helper::post_val("firebase_auth");

        if($site_config->where(["id" => $id])->update()){

            $response_obj["firebase_push"]["text"] = $site_config->to_valid_array();
            $response->create(200, "Success", $response_obj);

        }else $response->create(201, "Something went wrong.", null);

    } else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();
?>