<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $page = Helper::get_val("page");
        if(!$page) $page = 1;

        $start = ($page - 1) * BACKEND_PAGINATION;

        $search = Helper::get_val("search");
        $sort = Helper::get_val("sort");
        $sort_type = Helper::get_val("sort_type");

        if(!$sort_type) $sort_type = "DESC";

        $app_feedback_count = new App_Feedback();

        $has_search = ($search && ($search != "") && ($search != null));
        $has_sort = ($sort && ($sort != "") && ($sort != null));


        if($has_search){
            $app_feedback_count = $app_feedback_count->where(['admin_id' => $admin->id])
                ->like(["email" => $search])->search()->count();
        } else {
            $app_feedback_count = $app_feedback_count->where(['admin_id' => $admin->id])->count();
        }



        $response_values = [];

        $response_obj["total"] = $app_feedback_count;
        $response_obj["page"] = $page;
        $response_obj["page_item"] = BACKEND_PAGINATION;

        $app_feedbacks = new App_Feedback();

        if($has_search && $has_sort){
            $app_feedbacks = $app_feedbacks->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->like(["email" => $search])->search()
                ->orderBy($sort)->orderType($sort_type)->all();

        }else if($has_sort){
            $app_feedbacks = $app_feedbacks->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->orderBy($sort)->orderType($sort_type)->all();

        }else if($has_search){
            $app_feedbacks = $app_feedbacks->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->like(["email" => $search])->search()
                ->orderBy("created")->orderType("DESC")
                ->all();

        } else {
            $app_feedbacks = $app_feedbacks->where(['admin_id' => $admin->id])->limit($start, BACKEND_PAGINATION)
                ->orderBy("created")->orderType("DESC")
                ->all();
        }

        $response_obj["current_item_count"] = count($app_feedbacks);


        foreach ($app_feedbacks as $item){


            $current_values["email"]["text"] = (!empty($item->email)) ? $item->email : "N/A";
            $current_values["feedback"]["text"] = (!empty($item->feedback)) ? $item->feedback : "N/A";
            $current_values["created"]["text"] = Helper::days_ago($item->created);

            $current_item["delete"] = $item->id;

            $current_item["values"] = $current_values;

            array_push($response_values, $current_item);
        }

        $response_obj["head"] = ["Email" => [ "email", "DESC"],
            "Feedback" => [ "feedback", "DESC" ],
            "Created" => [ "created", "ASC" ], "" => null ];
        $response_obj["body"] = $response_values;

        $response->create(200, "Success", $response_obj);

    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>