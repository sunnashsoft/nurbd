<?php require_once('../../../private/init.php'); ?>

<?php
$response = new Response();
$admin = Session::get_session(new Admin());

if(!empty($admin)) {
    if(Helper::is_get()){

        $id = Helper::get_val("id");

        if($id){
            $app_feedback = new App_Feedback();
            $app_feedback = $app_feedback->where(["id" => $id])->one();
            
            if(!empty($app_feedback)){
                if($app_feedback->admin_id == $admin->id){

                    $delete_app_feedback = new App_Feedback();
                    if($delete_app_feedback->where(["id" => $id])->delete()){

                        $response->create(200, "Success", $app_feedback);

                    }else $response->create(201, "Something went wrong. Please try again.", null);
                }else $response->create(201, "You are unable to delete this feedback", null);
            }else $response->create(201, "Invalid Feedback", null);
        }else $response->create(201, "Invalid Parameter", null);
    }else $response->create(201, "Invalid Request Method", null);
}else $response->create(201, "Please log in", null);

echo $response->print_response();

?>