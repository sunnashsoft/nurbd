<?php require_once('../private/init.php'); ?>

<?php
$admin = Session::get_session(new Admin());
if(empty($admin)) Helper::redirect_to("login.php");
else {
	$setting = new Setting();
	$setting = $setting->where(["admin_id"=> $admin->id])->one();
}
?>

<?php require("common/php/php-head.php"); ?>
<body>
<?php require("common/php/header.php"); ?>
<div class="main-container">

	<?php require("common/php/sidebar.php"); ?>

	<div class="main-content">
		<div class="item-wrapper three">
			<div class="item item-dahboard">
				<div class="item-inner">

					<?php
						$artists = new Artist();
						$artists = $artists->where(["admin_id" => $admin->id])->count();
					?>
					<div class="item-content">
						<h2 class="title"><b><?php echo $artists; ?></b></h2>
						<h4 class="desc">Artist</h4>
					</div><!--item-content-->

					<div class="icon"><i class="ion-android-film"></i></div>

					<div class="item-footer">
						<a href="artists.php">More info <i class="ml-10 ion-chevron-right"></i>
							<i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->

				</div><!--item-inner-->
			</div><!--item-->

			<?php $albums = new Album();
				$albums = $albums->where(["admin_id" => $admin->id])->count(); ?>

			<div class="item item-dahboard">
				<div class="item-inner">
					<div class="item-content">
						<h2 class="title"><b><?php echo $albums; ?></b></h2>
						<h4 class="desc">Album</h4>
					</div>
					<div class="icon"><i class="ion-ios-download"></i></div>
					<div class="item-footer">
						<a href="albums.php">More info <i class="ml-10 ion-chevron-right"></i><i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->
				</div><!--item-inner-->
			</div><!--item-->


			<?php $tracks = new Track();
				$tracks = $tracks->where(["admin_id" => $admin->id])->count(); ?>

			<div class="item item-dahboard">
				<div class="item-inner">
					<div class="item-content">
						<h2 class="title"><b><?php echo $tracks; ?></b></h2>
						<h4 class="desc">Track</h4>
					</div>
					<div class="icon"><i class="ion-social-buffer"></i></div>
					<div class="item-footer">
						<a href="tracks.php">More info <i class="ml-10 ion-chevron-right"></i><i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->
				</div><!--item-inner-->
			</div><!--item-->


			<?php $users = new User();
				$users = $users->where(["admin_id" => $admin->id])->count(); ?>


			<div class="item item-dahboard">
				<div class="item-inner">
					<div class="item-content">
						<h2 class="title"><b><?php echo $users; ?></b></h2>
						<h4 class="desc">User</h4>
					</div>
					<div class="icon"><i class="ion-android-laptop"></i></div>

					<div class="item-footer">
						<a href="users.php">More info <i class="ml-10 ion-chevron-right"></i><i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->

				</div><!--item-inner-->
			</div><!--item-->

			<?php $playlist = new Playlist();
			$playlist = $playlist->where(["admin_id" => $admin->id])->count(); ?>


			<div class="item item-dahboard">
				<div class="item-inner">
					<div class="item-content">
						<h2 class="title"><b><?php echo $playlist; ?></b></h2>
						<h4 class="desc">Playlist</h4>
					</div>
					<div class="icon"><i class="ion-card"></i></div>
					<div class="item-footer">
						<a href="playlists.php">More info <i class="ml-10 ion-chevron-right"></i><i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->
				</div><!--item-inner-->
			</div><!--item-->


			<?php

			$song_played = new Track();
			$song_played = $song_played->where(["admin_id" => $admin->id])->sum("view_count");
			?>

			<div class="item item-dahboard">
				<div class="item-inner">
					<div class="item-content">
						<h2 class="title"><b><?php echo $song_played; ?></b></h2>
						<h4 class="desc">Song Played by Users</h4>
					</div>
					<div class="icon"><i class="ion-android-star-half"></i></div>
					<div class="item-footer">
						<a href="tracks.php">More info <i class="ml-10 ion-chevron-right"></i><i class="ion-chevron-right"></i></a>
					</div><!--item-footer-->

				</div><!--item-inner-->
			</div><!--item-->
		</div><!--item-wrapper-->


	</div><!--main-content-->
</div><!--main-container-->


<?php require("common/php/php-footer.php"); ?>

